# -*- coding: utf-8 -*-
# See LICENSE file for full copyright and licensing details.
from odoo import models, fields


class Program(models.Model):
    _name = 'core.program'

    name = fields.Char("Program Name", required=True)
    program_code = fields.Char('Code', required=True)
    program_type = fields.Selection(
        [('l', 'License'), ('m', 'Master'), ('d', 'Doctorat')],
        'Program Type', required=True)

    service_id = fields.Many2one( 'core.service', 'Service', required=True)


class Speciality(models.Model):
    _name = 'core.speciality'

    name = fields.Char("Speciality Name", required=True)
    speciality_code = fields.Char('Code')

    year_ids = fields.One2many(
        'core.speciality.year', 'speciality_id', 'Years')


class Years(models.Model):
    _name = 'core.speciality.year'

    name = fields.Char('Name', required=True)
    year_code = fields.Char('Code')

    next_year_id = fields.Many2one(
        'core.speciality.year','next year pointer')
    speciality_id = fields.Many2one(
        'core.speciality', 'Speciality')
    semester_ids = fields.One2many(
        'core.speciality.year.semester', 'year_id', 'semesters')
    program_id = fields.Many2one(
        'core.program','Program')
    inscription_id = fields.Many2one(
        'core.student.ins_year','inscription')

class Semester(models.Model):
    _name = 'core.speciality.year.semester'

    name = fields.Char('Name', required=True)
    semester_code = fields.Char('Code')

    year_id = fields.Many2one('core.speciality.year', 'Year', required=True)


class Unit(models.Model):
    _name = 'core.speciality.year.semester.unit'

    name = fields.Char('Name')
    credit = fields.Float('Credit')
    coeff = fields.Float('Coefficient')
    unit_code = fields.Char('Code')

    semester_id = fields.Many2one(
        'core.speciality.year.semester', 'Semester', required=True)
    unit_type_id = fields.Many2one(
        'core.speciality.year.semester.unit.type', "Unit Type", required=True)

    course_ids = fields.One2many('core.speciality.year.semester.unit.course', 'unit_id', 'Courses List') # a ajouter dans la vue

class UnitType(models.Model):
    _name = 'core.speciality.year.semester.unit.type'

    name = fields.Char('Name', required=True)
    unit_type_code = fields.Char('Code')
    unit_ids = fields.One2many(
        'core.speciality.year.semester.unit', 'unit_type_id', 'Unit')


class Course(models.Model):
    _name = 'core.speciality.year.semester.unit.course'

    name = fields.Char('Name', required=True)
    course_code = fields.Char('Code')
    credit = fields.Float('Credit')

    coeff = fields.Float('Coefficient')
    is_exam = fields.Boolean('Has an exam ')
    is_ratrappage = fields.Boolean('Has a second exam ')
    is_course = fields.Boolean('Has a lecturer ')
    is_td = fields.Boolean('Has a TD ')
    is_tp = fields.Boolean('Has a TP ')
    obligatory = fields.Boolean('Is This Course Obligatory',required=True)
    unit_id = fields.Many2one(
        'core.speciality.year.semester.unit', 'Unit', required=True)
    course_responsible_ids = fields.One2many(
        'core.teacher.teacher_rsp_course', 'course_id', 'Responsibles')
