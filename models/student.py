# -*- coding: utf-8 -*-
from odoo import models, fields


class Student(models.Model):
    _name = 'core.student'

    registration_number = fields.Char(
        'Student registration number ', required=True)
    name_student = fields.Char('Student name ', required=True)
    surname_student = fields.Char('Student surname ', required=True)
    name_arb = fields.Char('Student name arabic', required=True)
    surname_student_arb = fields.Char('student surname arabic')
    birthday = fields.Date('birthday ', required=True)
    birthday_place = fields.Char('Birthday place', required=True)
    picture = fields.Binary('Student Picture')
    sex = fields.Selection([('M', 'Male'), ('F', 'Female')],
                           'Gender', default='M', required=True)

    nationality = fields.Char('Nationality', required=True)
    inscription_type = fields.Selection([('de', 'Dette'),
                                         ('n', 'New'), ('do', 'Ajourne(e)')],
                                        'inscription type', default='n',
                                        required=True)

    bac_id = fields.Integer('Bac ID', size=10, required=True)
    bac_year = fields.Integer('Bac year', size=4, required=True)
    serie_bac_id = fields.Char('Serie number of bac')
    bac_mention = fields.Integer('Bac mention')
    bac_note = fields.Integer('Note bac')
    bac_wilaya = fields.Char('Bac wiliya')
    graduation_code = fields.Char('Graduation code')  # a voir
    year = fields.Integer('First inscrption year', size=4, required=True)
    interne = fields.Boolean('Interne ?')
    bourse = fields.Boolean('Bourse ? ')
    redoubling = fields.Boolean('redoubling ? ')
    year_cycle = fields.Integer('Year cycle ', size=4, required=True)
    sanction = fields.Char('Sanction ')
    study_year = fields.Char('Study year ')
    study_cycle = fields.Char('Cycle year ', required=True)
    study_filiere = fields.Char('Filiere Study ', required=True)
    orientation = fields.Boolean('oriented ? ')  # avoir
    grade = fields.Boolean('grade ? ')  # avoir
    orientation_year = fields.Char('orientation_year')
    orientation_cycle = fields.Char('orientation_cycle ')
    orientation_filiere = fields.Char('orientation_cycle ')
    surname_father = fields.Char('Surname Father')
    name_surname_mother = fields.Char(
        'name and suranme of the mother ')
    transfert = fields.Boolean(
        'transfert from university to another ? ', required=True)
    transfert_description = fields.Text('transfert description')
    orientation_status = fields.Char(
        'verifie if the student is realy oriented ') # a voire avec une fonction
    totale_number = fields.Char('totale number of redoublement')
    n_ins_anc = fields.Char('N Ins_Anc ')
    birthday_place_french = fields.Char('birthday place fr')
    nationality_french = fields.Char(' nationality fr')
    dette_number = fields.Char('nombre de dettes')
    session_2_nmbr = fields.Char('session 2 number')

    
    address_id = fields.Many2one('core.address', "Address")
    contact_id = fields.Many2one('core.contact', 'Contact Information')

    faculty_id = fields.Many2one('core.faculty', 'Faculty')  # a voir #
    registration_year_ids = fields.One2many(
        'core.student.ins_year', 'student_id', string="Student Registrations")
