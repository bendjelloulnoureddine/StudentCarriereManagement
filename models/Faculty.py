# -*- coding: utf-8 -*-
from odoo import models, fields


class Faculty(models.Model):
    _name = 'core.faculty'

    name = fields.Char('Faculty', required=True)
    code_faculty = fields.Char('Faculty Code', required=True)
    faculty_ara = fields.Char('Arabic Faculty Name')
    description = fields.Text('Description')
    description_faculty_ar = fields.Text('Arabic Description')

    university_id = fields.Many2one('res.company', 'University', required=True)
    address_id = fields.Many2one('core.address', "Address")
    contact_id = fields.Many2one('core.contact', 'Contact Information')


class Domaine(models.Model):
    _name = 'core.domaine'

    name = fields.Char('Title', required=True)
    code_domaine = fields.Char('Code', required=True)
    domaine_ar = fields.Char('Domain Arabic Name', required=True)
    description = fields.Text('Description')
    description_domaine_ar = fields.Text('Arabic Description')

    faculty_id = fields.Many2one(
        'core.faculty', 'Faculty', required=True)

    service_ids = fields.One2many('core.service', 'domaine_id', 'Services') # a ajouter dans la vue

class Service(models.Model):
    _name = 'core.service'

    name = fields.Char('name of service ',required=True)
    name_arb = fields.Char('Arabic name service ')
    service_code = fields.Char('Service code ')
    description = fields.Text('Description')
    description_service_ar = fields.Text('Description Arabic')

    department_id = fields.Many2one(
        'core.department','Departement')

    domaine_id = fields.Many2one('core.domaine', 'Domaine')

class Departement (models.Model):
    _name = 'core.department'

    name = fields.Char('Department', required=True)
    name_arabic = fields.Char('Department Arabic Name')
    code_departement = fields.Char('Department Code', required=True)
    description = fields.Text('Description')
    description_dept_ar = fields.Text('Arabic Description')

    contact_id = fields.Many2one(
        'core.contact', 'Contact Information')
    partener_id = fields.Many2one(
        'res.users','User')
    service_ids = fields.One2many(
        'core.service', 'department_id', 'Services')

class Address (models.Model):
    _name = 'core.address'

    street = fields.Char('Street')
    street1 = fields.Char('Street 2')
    zip_code = fields.Integer('Zip Code')

    state_id = fields.Many2one(
        'res.country.state', 'State')
    country_id = fields.Many2one(
        'res.country', 'Country')
    commune_id = fields.Many2one(
        'res.country.state.commune', 'Commune') #a voire

class ContactInformation(models.Model):
    _name = 'core.contact'

    phone = fields.Char('Home Phone Number')
    mobile = fields.Char('Mobile Number')
    fax = fields.Char('Fax')
    email = fields.Char('Email Address')
    website = fields.Char('Website')
