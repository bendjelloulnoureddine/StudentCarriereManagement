# -*- coding: utf-8 -*-
from odoo import models, fields


class Teacher(models.Model):
    _name = 'core.teacher'

    teacher_code = fields.Char('teacher id', required=True)
    name = fields.Char('name', required=True)
    surname = fields.Char('surname', required=True)
    sex = fields.Selection([('M', 'Male'), ('F', 'Female')],
                           ' Gender', default='M', required=True)

    picture = fields.Binary('Teacher Picture')

    grade = fields.Selection([('MAB', 'Maitre Assistant B'),
                              ('MAA', 'Maitre Assistant A'),
                              ('MDCB', 'Maitre Conference B'),
                              ('MDCA', 'Maitre Conference A'),
                              ('PB', 'Professeur B'),
                              ('PA', 'Professeur A')], ' Grade',
                             default='MAB', required=True)

    post_administratif = fields.Selection([
        ('Aucun', 'Aucun'), ('AD', 'Adjoint Chef Departement'),
        ('CD', 'Chef Departement'),
        ('CF', 'Chef Filiere'),
        ('VD', 'Vice Doyen'),
        ('D', 'Doyen')], 'Post Administratif', default='Aucun', required=True)

    teacher_type = fields.Selection([('V', 'Vacataire'), ('T', 'Titulaire')],
                                    'Type', default='V', required=True)

    fonction_ref = fields.Char('fonction ref')  # a voir

    total_charges = fields.Char('charge totale')  # a voir
    course_charge = fields.Char('charge course ')  # a voir
    td_charge = fields.Char('charge TD')  # a voir
    categorie = fields.Char('categorie ? ')  # a voir
    tot_tp = fields.Char('tot tp ? ')  # a voir
    tot_td = fields.Char('tot td ? ')  # a voir
    admin_or_not = fields.Boolean('admin / not', required=True)
    password_teacher = fields.Char('password teacher', required=True)

    res_course_ids = fields.One2many(
        'core.teacher.teacher_rsp_course',
        'teacher_id', "Courses' Responsible")
    departement_ref = fields.Many2one(
        'core.department', 'Department Ref', required=True)


class TeacherRspCourse(models.Model):
    _name = 'core.teacher.teacher_rsp_course'

    coef_exam = fields.Float('Exam Coef')
    coef_cc = fields.Float('Evaluation Coef')

    univ_year_id = fields.Many2one(
        'core.univ_year', 'Year')
    course_id = fields.Many2one(
        'core.speciality.year.semester.unit.course', 'Course')
    associate_teacher_ids = fields.One2many(
        'core.teacher', "res_course_ids")
    teacher_id = fields.Many2one(
        'core.teacher', 'Teacher')
