#-*- Bendjelloul Noureddine  ~/~ Odoo Project ~/~  -*- #
#-*- coding: utf-8 -*-


#
##############################################################################
#
#    Cybrosys Technologies Pvt. Ltd.
#    Copyright (C) 2017-TODAY Cybrosys Technologies(<https://www.cybrosys.com>).
#    Author: Nikhil krishnan(<https://www.cybrosys.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <https://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'Faculty management',
    'version': '10.0.2.0',
    'summary': "Faculty Management Module",
    'author': 'University of Mostaganem',
    'company': 'FSEI',
    'website': '',
    'category': 'Academic',
    'depends': ['base'],
    'license': 'LGPL-3',
    'data': [
        'views/Faculty.xml',
        'views/Program.xml',
        'views/student.xml',
        'views/teacher.xml',
        'views/result.xml',
    ],
    'demo': [],
    'images': ['static/description/logo.png'],
    'installable': True,
    'auto_install': False,
}
