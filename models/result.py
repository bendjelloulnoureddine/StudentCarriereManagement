# -*- coding: utf-8 -*-
from odoo import models, fields, api


class UnivYear(models.Model):
    _name = 'core.univ_year'

    name = fields.Char('University Year')
    resp_course_ids = fields.One2many(
        'core.teacher.teacher_rsp_course', 'univ_year_id')

    @api.depends('univ_year_id','insc_univ_year','year','inscription_course_ids','student_id','year_id')
    def year_creation(self):
        for student in self.env['student_id'].browse() :

            inscription_id = self.env['core.student.year'].create({
                'univ_year_id':self.id,
                'insc_univ_year' : 1,
                'student_id' :student.id
                 })

        for semester in self.env['core.speciality.year.semester'].search([('year_id','=', 1)]) :
            for unit in self.env['core.speciality.year.semester.unit'].search([('semester_id','=',semester.id)]) :
                for course in self.env['core.speciality.year.semester.unit.course'].search([('unit_id','=',unit.id)]) :
                        self.env['core.student.ins_course'].create({
                            'coeff_exam' : 3.0,
                            'coeff_cc' : 5.0 ,
                            'course_id' : Course.id ,
                            'ins_year_id' : inscription_id.id
                            })


class InsYear(models.Model):
    _name = 'core.student.ins_year'

    group_ref = fields.Char('Group ref')
    section_ref = fields.Char('Section ref')
    general_avg = fields.Float('General average')
    observation = fields.Char('Observation')

    univ_year_id = fields.Many2one(
        'core.univ_year', 'University Year')
    inscription_course_ids = fields.One2many(
        'core.student.ins_course', 'ins_year_id', "Course Registrations")
    student_id = fields.Many2one(
        'core.student', 'Student')
    insc_univ_year = fields.Many2one(
        'core.speciality.year', 'Speciality Year')#11


class InsCourse(models.Model):
    _name = 'core.student.ins_course'

    def _get_coef_course(self):

        responsable_course = self.env['core.teacher.teacher_rsp_course'].search([('univ_year_id', '=',self.ins_year_id.univ_year_id.id ),('course_id', '=', self.course_id.id)])
        self.coeff_exam = responsable_course.coef_exam
        self.coeff_cc = responsable_course.coef_cc

    coeff_exam = fields.Float('coeff exam ', required=True, compute='_get_coef_course')
    coeff_cc = fields.Float('coeff CC ', required=True, compute='_get_coef_course')

    result_line_ids = fields.One2many(
        'core.result_unit.result_semestre.result_ligne', 'inscription_course_id', 'Cours results')
    course_id = fields.Many2one(
        'core.speciality.year.semester.unit.course', 'Course')
    result_unit_id = fields.Many2one('core.result_unit', 'result unit')
    ins_year_id = fields.Many2one(
        'core.student.ins_year', "Student Inscription")



    @api.depends('result_line_ids')
    def compute_avg_cc(self):
        avg_cc = False
        cc_count = False
        for line in self.result_line_ids:
            if line.evaluation_type == 'cc':
                avg_cc = avg_cc + line.mark
                cc_count = cc_count + 1
            self.evaluation_cc = avg_cc / cc_count

    @api.depends('result_line_ids')
    def compute_exam(self):
        exam = False
        if self.env['core.result_unit.result_semestre.result_ligne'].search(
                [('evaluation_type', '=', 'ratrappage'), ('inscription_course_id', '=', self.id)]):
            evaluation_exam = self.env['core.result_unit.result_semestre.result_ligne'].search(
                [('evaluation_type', '=', 'ratrappage'), ('inscription_course_id', '=', self.id)]).mark
        else:
            self.env['core.result_unit.result_semestre.result_ligne'].search(
                [('evaluation_type', '=', 'exam'), ('inscription_course_id', '=', self.id)]).mark

    @api.depends('result_line_ids')
    def compute_avg(self):
        self.average_mark = (
            self.evaluation_exam * self.coeff_exam) + (self.evaluation_cc * self.coeff_cc)

    evaluation_cc = fields.Float('Avg cc ', compute='compute_avg_cc', store=True)
    evaluation_exam = fields.Float('Exam ', compute='compute_exam', store=True)
    average_mark = fields.Float('Average ', compute='compute_avg', store=True)


class ResultUnit(models.Model):
    _name = 'core.result_unit'

    n_inscription = fields.Char('n inscription', required=True)
    code_ue = fields.Char('code ue', required=True)
    credit_gain = fields.Integer('credit obtebu', required=True)
    avrg_eu = fields.Char('avrg_eu', required=True)
    aquit = fields.Boolean('aquit ', required=True)
    exlu_eu = fields.Boolean('Exclu  ', required=True)
    unit_avr_mark = fields.Float(
        'unit avrg mark', compute='unit_avg', store=True)
    coef_all = fields.Float('coeff all ', compute='unit_avg', store=True)

    result_semestre_id = fields.Many2one(
        'core.result_unit.result_semestre', 'ResultSemestre', required=True)
    ins_course_ids = fields.One2many(
        'core.student.ins_course', 'result_unit_id', 'inscription course')

    @api.depends('ins_course_ids')
    def unit_avg(self):
        coeff_sum = False
        cc_count = False
        for line in self.result_line_ids:
            if line.evaluation_type == 'cc':
                avg_cc = avg_cc + line.mark
                cc_count = cc_count + 1
            self.evaluation_cc = avg_cc / cc_count

    @api.depends('ins_course_ids')
    def unit_avg(self):
        coeff_sum = False

        mark_sum = False
        for course in self.ins_course_ids:
            coeff_sum = coeff_sum + course.course_id.coeff
            mark_sum += course.average_mark * course.course_id.coeff
        self.unit_avr_mark = mark_sum / coeff_sum
        self.coeff_sum = coeff_sum


class ResultSemestre(models.Model):
    _name = 'core.result_unit.result_semestre'

    name = fields.Char("Name")
    mark = fields.Float("Mark Average")

    result_unit_ids = fields.One2many(
        'core.result_unit', 'result_semestre_id', required=True)

    @api.depends('result_unit_ids')
    def semester_avg(self):
        sum_unit = False
        for unit in self.result_unit_ids:
            sum_unit = unit.unit_avr_mark
        self.semester_avg_mark = sum_unit / coeff_sum

    semester_avg_mark = fields.Float(
        'semester average', compute='semester_avg', store=True)


class ResultLigne(models.Model):
    _name = 'core.result_unit.result_semestre.result_ligne'


    name = fields.Char('Evaluation Name')
    evaluation_type = fields.Selection(
        [('exam', 'Exam'), ('ratrappage', 'Ratrappage'), ('cc', 'Control Contenu')], 'evaluation_type')
    mark = fields.Float('Mark')
    date = fields.Date('Evaluation Date')

    inscription_course_id = fields.Many2one(
        'core.student.ins_course', required=True)
